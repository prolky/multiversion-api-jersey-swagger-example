# README #

Accompanying source code for blog entry at http://tech.asimio.net/2016/05/07/Documenting-multiple-REST-API-versions-using-Spring-Boot-Jersey-and-Swagger.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Docker host or Docker machine

### Building and executing the application from command line ###

```
mvn clean package
java -jar target/multiversion-api-jersey-swagger.jar
```

Open http://localhost:8000 in a browser

### How do I get set up using Docker? ###

```
sudo docker pull asimio/multiversion-api-jersey-swagger
sudo docker run -idt -p 8702:8702 -e appPort=8702 asimio/multiversion-api-jersey-swagger:latest
```

Open http://localhost:8702 in a browser

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero