package com.asimio.swaggerexample.rest.v2;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.asimio.swaggerexample.main.Application;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class HelloResourceV2Test {

	private static final String API_PATH = "/api";
	private static final String MSG1_TEMPLATE = "Hello %s. Version %s - passed in %s";
	private static final String MSG2_TEMPLATE = "Hello again %s. Version %s - passed in %s";

	@Value("${local.server.port}")
	private int port;

	@Before
	public void setup() {
		RestAssured.port = this.port;
	}

	@Test
	public void shouldRetrieveNameVersion2InURL() {
		String name = "world";
		RestAssured.
			given().
				accept(ContentType.JSON).
			when().
				get(String.format("%s/v2/hello/{name}", API_PATH), name).
			then().
				statusCode(HttpStatus.SC_OK).
				contentType(ContentType.JSON).
				body("msg1", Matchers.equalTo(String.format(MSG1_TEMPLATE, name, 2, "URL"))).
				body("msg2", Matchers.equalTo(String.format(MSG2_TEMPLATE, name, 2, "URL")));
	}

	@Test
	public void shouldRetrieveNameVersion2InAcceptHeader() {
		String name = "world";
		RestAssured.
			given().
				accept("application/vnd.asimio-v2+json").
			when().
				get(String.format("%s/hello/{name}", API_PATH), name).
			then().
				statusCode(HttpStatus.SC_OK).
				contentType("application/vnd.asimio-v2+json").
				body("msg1", Matchers.equalTo(String.format(MSG1_TEMPLATE, name, 2, "Accept Header"))).
				body("msg2", Matchers.equalTo(String.format(MSG2_TEMPLATE, name, 2, "Accept Header")));
	}

	@Test
	public void retrieveShouldResultIn404Version2InURL() {
		String name = "404";
		RestAssured.
			when().
				get(String.format("%s/v2/hello/{name}", API_PATH), name).
			then().
				statusCode(HttpStatus.SC_NOT_FOUND).
				contentType(ContentType.JSON);
	}

	@Test
	public void retrieveShouldResultIn404Version2InAcceptHeader() {
		String name = "404";
		RestAssured.
			given().
				accept("application/vnd.asimio-v2+json").
			when().
				get(String.format("%s/hello/{name}", API_PATH), name).
			then().
				statusCode(HttpStatus.SC_NOT_FOUND).
				contentType(ContentType.JSON);
	}

	@Test
	public void shouldCreateNewResourceVersion2InUrl() {
		RestAssured.
			given().
				contentType("application/json").
				accept("application/json").
				body("{ \"msg1\": \"world\", \"msg2\": \"world again\" }").
			when().
				post(String.format("%s/v2/hello", API_PATH)).
			then().
				statusCode(HttpStatus.SC_CREATED).
				header("Location", Matchers.equalTo(String.format("http://localhost:%s%s/v2/hello/%s--%s", this.port, API_PATH, "world", "world%20again")));
	}

	@Test
	public void shouldCreateNewResourceVersion2InAcceptHeader() {
		RestAssured.
			given().
				contentType("application/vnd.asimio-v2+json").
				body("{ \"msg1\": \"world\", \"msg2\": \"world again\" }").
			when().
				post(String.format("%s/hello", API_PATH)).
			then().
				statusCode(HttpStatus.SC_CREATED).
				header("Location", Matchers.equalTo(String.format("http://localhost:%s%s/hello/%s--%s", this.port, API_PATH, "world", "world%20again")));
	}
}