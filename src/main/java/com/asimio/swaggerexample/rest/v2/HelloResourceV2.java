package com.asimio.swaggerexample.rest.v2;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.asimio.swaggerexample.model.v2.Hello;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@Component
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "Hello resource", produces = "application/json")
public class HelloResourceV2 {

	private static final Logger LOGGER = LoggerFactory.getLogger(HelloResourceV2.class);

	@GET
	@Path("v2/hello/{name}")
	@ApiOperation(value = "Gets a hello resource. Version 2 - (version in URL)", response = Hello.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Hello resource found"),
	    @ApiResponse(code = 404, message = "Hello resource not found")
	})
	public Response getHelloVersionInUrl(@ApiParam @PathParam("name") String name) {
		LOGGER.info("getHelloVersionInUrl() v2");
		return this.getHello(name, "Version 2 - passed in URL");
	}

	@GET
	@Path("hello/{name}")
	@Consumes("application/vnd.asimio-v2+json")
	@Produces("application/vnd.asimio-v2+json")
	@ApiOperation(value = "Gets a hello resource. World Version 2 (version in Accept Header)", response = Hello.class)
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Hello resource found"),
	    @ApiResponse(code = 404, message = "Hello resource not found")
	})
	public Response getHelloVersionInAcceptHeader(@PathParam("name") String name) {
		LOGGER.info("getHelloVersionInAcceptHeader() v2");
		return this.getHello(name, "Version 2 - passed in Accept Header");
	}

	@POST
	@Path("v2/hello")
	@ApiOperation(value = "Creates hello resource. Version 2 - (version in URL)", response = Hello.class)
	@ApiResponses(value = {
		@ApiResponse(code = 201, message = "hello resource created", responseHeaders = {
			@ResponseHeader(name = "Location", description = "The URL to retrieve created resource", response = String.class)
		})
	})
	public Response createHelloVersionInUrl(Hello hello, @Context UriInfo uriInfo) {
		LOGGER.info("createHelloVersionInUrl() v2");
		return this.createHelloWorld(hello, uriInfo);
	}

	@POST
	@Path("hello")
	@Consumes("application/vnd.asimio-v2+json")
	@ApiOperation(value = "Creates hello resource. Version 2 - (version in Accept Header)", response = Hello.class)
	@ApiResponses(value = {
		@ApiResponse(code = 201, message = "hello resource created", responseHeaders = {
			@ResponseHeader(name = "Location", description = "The URL to retrieve created resource", response = String.class)
		})
	})
	public Response createHelloVersionInAcceptHeader(Hello hello, @Context UriInfo uriInfo) {
		LOGGER.info("createHelloVersionInAcceptHeader() v2");
		return this.createHelloWorld(hello, uriInfo);
	}

	private Response getHello(String name, String partialMsg) {
		if ("404".equals(name)) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Hello result = new Hello();
		result.setMsg1(String.format("Hello %s. %s", name, partialMsg));
		result.setMsg2(String.format("Hello again %s. %s", name, partialMsg));
		return Response.status(Status.OK).entity(result).build();
	}

	private Response createHelloWorld(Hello hello, UriInfo uriInfo) {
		// Creates resource and return 201 with reference to new resource in Location header
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.path(String.format("%s--%s", hello.getMsg1(), hello.getMsg2()));
		return Response.created(builder.build()).build();
	}
}