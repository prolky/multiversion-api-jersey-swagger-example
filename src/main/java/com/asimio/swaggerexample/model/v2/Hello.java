package com.asimio.swaggerexample.model.v2;

public class Hello {

	private String msg1;
	private String msg2;

	public String getMsg1() {
		return this.msg1;
	}

	public void setMsg1(String msg1) {
		this.msg1 = msg1;
	}

	public String getMsg2() {
		return this.msg2;
	}

	public void setMsg2(String msg2) {
		this.msg2 = msg2;
	}
}