package com.asimio.swaggerexample.web;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.asimio.swaggerexample.main.Application;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class StaticMediaTest {

	@Value("${local.server.port}")
	private int port;

	@Before
	public void setup() {
		RestAssured.port = this.port;
	}

	@Test
	public void shouldRetrieveSwaggerFileV1() {
		this.retrieveSwaggerFile("/v1/swagger.json");
	}

	@Test
	public void shouldRetrieveSwaggerFileV2() {
		this.retrieveSwaggerFile("/v2/swagger.json");
	}

	private void retrieveSwaggerFile(String filePath) {
		RestAssured.
			given().
				accept(ContentType.ANY).
			when().
				get(filePath).
			then().
				statusCode(HttpStatus.SC_OK);
	}
}