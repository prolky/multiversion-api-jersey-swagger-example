package com.asimio.swaggerexample.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		// Register endpoints, providers, ...
		this.registerEndpoints();
	}

	private void registerEndpoints() {
		this.register(com.asimio.swaggerexample.rest.v1.HelloResourceV1.class);
		this.register(com.asimio.swaggerexample.rest.v2.HelloResourceV2.class);
		// Access through /<Jersey's servlet path>/application.wadl
		this.register(WadlResource.class);
	}
}